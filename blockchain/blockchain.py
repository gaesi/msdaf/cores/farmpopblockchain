# import datetime
from datetime import datetime
import hashlib
import json


# Part 1, create blockchain
class Blockchain:
    def __init__(self):
        # blocks history
        self.proof = 1
        self.block = {}
        self.chain = []
        self.challenge = '0000'

        # genesis block
        self.create_block(proof=1, previous_hash='0')

    def create_block(self, proof, previous_hash):
        """
            Mine the next block
            :param proof: proof of work
            :param previous_hash: to connect the created block to it (previous block)
            :return:
        """
        block = {
            # Block: #1
            'index': len(self.chain) + 1,
            'timestamp': str(datetime.now()),
            # Nonce: 533
            'proof': proof,
            'previous_hash': previous_hash
        }

        # if not the genesis try to check if it block is valid, preventing inconsistency
        if len(self.chain) > 0:
            block['hash'] = self.calc_hash(proof=proof, prev_proof=self.chain[len(self.chain) - 1]['proof'])
            if block['hash'][:len(self.challenge)] == self.challenge:
                self.chain.append(block)
            else:
                block['hash'] = 'invalid'
        else:
            # genesis block
            self.chain.append(block)

        return block

    def get_previous_block(self):
        """
            :return: last chain block
        """
        return self.chain[-1]

    def proof_of_work(self, previous_proof=None):
        """
            Resolve the challenge to find the number to create the next block (the proof param of create_block function)
            It number must be hard to mining and easy to verify it number (proof)
            :param previous_proof:
            :return: the proof number passed to the create_block function
        """
        if previous_proof is None:
            previous_proof = self.proof

        new_proof = 1
        check_proof = False
        while check_proof is False:
            # generate it hash combining the previous_proof & it new_proof
            hash_operation = self.calc_hash(proof=new_proof, prev_proof=previous_proof)

            # how many 0s at the start (left) more hard it's to mining. 4 0s is an easy challenge
            if hash_operation[:len(self.challenge)] == self.challenge:
                check_proof = True
            else:
                new_proof += 1

        return new_proof

    def hash(self, block=None):
        """
            It generate the hash foreach block of the chain to the verification of the chain
            :param block: (can be self.current_block, can be useless to pass it params)
            :return: block json encoded in a hash
        """
        if block is None:
            block = self.block
        encoded_block = json.dumps(block, sort_keys=True).encode()
        return hashlib.sha256(encoded_block).hexdigest()

    def calc_hash(self, block_i=None, prev_block_i=None, proof=None, prev_proof=None, chain=None):
        """
        :param block_i: the block index to retrieve it proof and it prev block proof and calc it correlation
        :param prev_block_i: if trying to find new hash (mining) it prev_block must be passed
        :param chain: the chain of blocks, if None it is going to use the self chain
        :return:
        """
        if chain is None:
            chain = self.chain

        if prev_block_i is None and prev_proof is None:
            prev_block_i = block_i - 1

        if block_i is not None:
            block = chain[block_i]
            prev_block = chain[prev_block_i]
            proof = block['proof']
            prev_proof = prev_block['proof']

        return hashlib.sha256(str(proof ** 2 - prev_proof ** 2).encode()).hexdigest()

    def is_chain_valid(self, chain=None):
        """
            Check the both needs to certify the chain is valid:
                1. proof_of_work foreach block is right (initializing with 0000);
                2. check if prev_hash is equals the real prev_hash, it means if the chain of blocks is valid (not edited)
            :param chain: (can be self.chain, can be useless to pass it params)
            :return: True if there is no problem in the blocks of the chain
        """
        if chain is None:
            chain = self.chain

        previous_block = chain[0]
        block_index = 1
        while block_index < len(chain):
            block = chain[block_index]

            # check if prev_hash is equals the real prev_hash, it means if the chain of blocks is valid (not edited)
            if block['previous_hash'] != self.hash(previous_block):
                return False

            # calc it hash
            hash_operation = self.calc_hash(block_index)

            # proof_of_work foreach block is right (initializing with 0000),
            # if not initializing with 0000 (challenge var) it is false
            if hash_operation[:len(self.challenge)] != self.challenge:
                return False

            previous_block = block
            block_index += 1
        return True
